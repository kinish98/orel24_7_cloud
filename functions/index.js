const func = require('firebase-functions');
const admin = require('firebase-admin');
const firebaseHelper = require("firebase-functions-helper");
const express = require("express");
const bodyParser = require("body-parser");

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello : Kamal  NIC : 980240010V  !");
// });


admin.initializeApp(func.config().firebase);
let db = admin.firestore();

const app = express();
const main = express();
const userCollection = 'userCollection';

main.use('/api/v1', app);
main.use(bodyParser.json());
main.use(bodyParser.urlencoded({ extended: false }));

// webApi is your functions name, and you will pass main as 
// a parameter
exports.webApi = func.https.onRequest(main);

// Add new contact
app.post('/userCollection', (req, res) => {
    try {
        let userCollection  = {
            name: req.body['name'],
            nic: req.body['nic']
        }

        //adding a new document in collection - usercollection with ID
        let setDoc = db.collection('userCollection').doc('userCollection');

        setDoc.set(userCollection);

        res.status(200).send('success');
        
        //let userRef = db.collection('userCollection').doc('UC001');           

        // let setWithOptions = userRef.set({
        //     capital : true
        // },  {merge:true});

        // let data = {
        //     stringExample  : 'Hello world',
        //     booleanExample : true,
        //     numberExample  : 3.12159265,
        //     dateExample    : admin.firestore.Timestamp.fromDate(new Date('February 18, 2020')),
        //     arrayExample   : [5, true, 'hello'],
        //     nullExample    : null,
        //     objectExample  : {
        //         a: 5,
        //         b: true
        //     }            
        // };

        // let setDoc = db.collection('data').doc('one').set(data);

        // db.collection('userCollection').doc('new-user-id').

        //Add a new document with a generated id

        // let addDoc = db.collection('userCollection').add({
        //     name : "Saman",
        //     nic : '870567712V'
        // }).then(ref =>{
        //     console.log('Added document with ID: ', ref.id);
        // })
      // for use the references later
      
    //   let newUserRef = db.collection('userCollection').doc();
    //   //later
    //   setDoc = newUserRef.set({

    //   });
        
         //return user;
        
        // const newDoc = await firebaseHelper.firestore
        //             .createNewDocument(db, userCollection, user);
        // res.status(201).send(`Operation Successful: ${newDoc.name}`);
    } catch (error) {
        res.status(400).send(JSON.stringify(error))
    }
    
    
})

//----saving data in the firebase cloud----

//import user SDK
//var admin = require("firebase-admin");

//get a database reference
// var db = admin.database();
// var ref = db.ref("server/saving-data/userCollection")

// var usersRef = ref.child("userCollection");
// usersRef.set({
//     userCollection:{
//         name:"Saman",
//         nic:"980534412V"
//     }
// });

// usersRef.child("userCollection").set({
//     name:"Saman",
//     nic:"980534412V"
// });




